﻿using System;
using ISOEncryptor.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ISOEncryptor.Tests
{
    [TestClass]
    public class MainTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var plaintext = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas efficitur tortor sapien.";
            var src = new CryptHelper();
            var dest = new CryptHelper();
            var ciphertext = src.Encrypt(plaintext, dest.PublicKey);
            var result = dest.Decrypt(ciphertext);
            Assert.AreEqual(plaintext, result);
        }

        [TestMethod]
        public void MyTestMethod()
        {
            var src = new CryptHelper();
            var result = src.ExportKeys("123456");
            Assert.IsNotNull(result);
        }
    }
}
