﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Text;

namespace ISOEncryptor.Library
{
    public class CryptHelper
    {
        #region Private constants
        private const int PARAMETERS_STRENGTH = 1024;
        private const int RANDOM_KEY_SIZE = 64;
        private const int PAYLOAD_SIZE = 16;
        private const int NONCE_SIZE = 16;
        private const int AEAD_MAC_SIZE = 128;
        #endregion

        #region Fields
        private AsymmetricCipherKeyPair _keys;
        private SecureRandom _random;
        #endregion

        #region Public properties
        public byte[] PublicKey
        {
            get
            {
                var k = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(_keys.Public);
                return k.ToAsn1Object().GetDerEncoded();
            }
        }

        public string PEM
        {
            get
            {
                var k = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(_keys.Public);
                using (var text_writer = new StringWriter())
                {
                    var pem_writer = new PemWriter(text_writer);
                    pem_writer.WriteObject(_keys.Public);
                    pem_writer.Writer.Flush();
                    return text_writer.ToString();
                }
            }
        }
        #endregion

        #region Constructors
        public CryptHelper()
        {
            _random = new SecureRandom();
            var r = new RsaKeyPairGenerator();
            r.Init(new KeyGenerationParameters(_random, PARAMETERS_STRENGTH));
            _keys = r.GenerateKeyPair();
        }

        public CryptHelper(string pemFileContents, string password)
        {
            if (string.IsNullOrWhiteSpace(pemFileContents))
            {
                throw new ArgumentNullException("Invalid PEM file contents", nameof(pemFileContents));
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("Invalid password", nameof(password));
            }
            _random = new SecureRandom();
            _keys = ImportKeys(pemFileContents, password);
        }
        #endregion

        #region Public methods
        public string Encrypt(string plaintext, byte[] targetPublicKey)
        {
            if (string.IsNullOrEmpty(plaintext))
            {
                throw new ArgumentNullException("Plaintext cannot be empty", nameof(plaintext));
            }
            if ((targetPublicKey == null) || (targetPublicKey.Length == 0))
            {
                throw new ArgumentNullException("A public key must be provided", nameof(targetPublicKey));
            }
            var plaintext_bytes = Encoding.UTF8.GetBytes(plaintext);
            var random_key = GenerateRandomBytes(RANDOM_KEY_SIZE);
            var salt = GenerateRandomBytes(RANDOM_KEY_SIZE);
            var hashed_key = DeriveBytes(random_key, salt);
            var payload = GenerateRandomBytes(PAYLOAD_SIZE);
            var encryption_key = RSAEncrypt(random_key, targetPublicKey);
            var ciphertext = AESGCMEncrypt(plaintext_bytes, hashed_key, payload);
            var result = new byte[encryption_key.Length + salt.Length + ciphertext.Length];
            encryption_key.CopyTo(result, 0);
            salt.CopyTo(result, encryption_key.Length);
            ciphertext.CopyTo(result, encryption_key.Length + salt.Length);
            var output = Convert.ToBase64String(result);
            return output;
        }

        public string Decrypt(string ciphertext)
        {
            if (string.IsNullOrEmpty(ciphertext))
            {
                throw new ArgumentNullException("A ciphertext must be provided", nameof(ciphertext));
            }
            var input = Convert.FromBase64String(ciphertext);
            var encrypted_key = SubArray(input, 0, 128);
            var salt = SubArray(input, 128, RANDOM_KEY_SIZE);
            var random_key = RSADecrypt(encrypted_key);
            var hashed_key = DeriveBytes(random_key, salt);
            var ciphertext_bytes = SubArray(input, 128 + RANDOM_KEY_SIZE, input.Length - (128 + RANDOM_KEY_SIZE));
            var plaintext = AESGCMDecrypt(ciphertext_bytes, hashed_key);
            var output = Encoding.UTF8.GetString(plaintext);
            return output;
        }

        public string ExportKeys(string password)
        {
            var result = string.Empty;
            using (var string_writer = new StringWriter())
            {
                var pem_writer = new PemWriter(string_writer);
                pem_writer.WriteObject(_keys.Private, "AES-128-CBC", password.ToCharArray(), _random);
                pem_writer.Writer.Flush();
                return string_writer.ToString();
            }
        }


        #endregion

        #region Internal implementation
        private byte[] RSAEncrypt(byte[] plaintext, byte[] publicKey)
        {
            var imported_key = PublicKeyFactory.CreateKey(publicKey);
            var rsa_engine = new Pkcs1Encoding(new RsaEngine());
            rsa_engine.Init(true, imported_key);
            var ciphertext = rsa_engine.ProcessBlock(plaintext, 0, plaintext.Length);
            return ciphertext;
        }

        private byte[] RSADecrypt(byte[] ciphertext)
        {
            var rsa_engine = new Pkcs1Encoding(new RsaEngine());
            rsa_engine.Init(false, _keys.Private);
            var plaintext = rsa_engine.ProcessBlock(ciphertext, 0, ciphertext.Length);
            return plaintext;
        }

        private byte[] AESGCMEncrypt(byte[] plaintext, byte[] key, byte[] payload)
        {
            var nonce = GenerateRandomBytes(NONCE_SIZE);
            var aesgcm_engine = new GcmBlockCipher(new AesEngine());
            var parameters = new AeadParameters(new KeyParameter(key), AEAD_MAC_SIZE, nonce, payload);
            aesgcm_engine.Init(true, parameters);
            var ciphertext_size = aesgcm_engine.GetOutputSize(plaintext.Length);
            var ciphertext = new byte[ciphertext_size];
            var ciphertext_length = aesgcm_engine.ProcessBytes(plaintext, 0, plaintext.Length, ciphertext, 0);
            aesgcm_engine.DoFinal(ciphertext, ciphertext_length);
            using (var output_stream = new MemoryStream())
            {
                using (var binary_writer = new BinaryWriter(output_stream))
                {
                    binary_writer.Write(payload);
                    binary_writer.Write(nonce);
                    binary_writer.Write(ciphertext);
                }
                return output_stream.ToArray();
            }
        }

        private byte[] AESGCMDecrypt(byte[] ciphertext, byte[] key)
        {
            using (var cipher_stream = new MemoryStream(ciphertext))
            using (var cipher_reader = new BinaryReader(cipher_stream))
            {
                var payload = cipher_reader.ReadBytes(PAYLOAD_SIZE);
                var nonce = cipher_reader.ReadBytes(NONCE_SIZE);
                var aesgcm_engine = new GcmBlockCipher(new AesEngine());
                var parameters = new AeadParameters(new KeyParameter(key), AEAD_MAC_SIZE, nonce, payload);
                aesgcm_engine.Init(false, parameters);
                var encrypted_text = cipher_reader.ReadBytes(
                    ciphertext.Length - (PAYLOAD_SIZE + NONCE_SIZE));
                var plaintext_size = aesgcm_engine.GetOutputSize(encrypted_text.Length);
                var plaintext = new byte[plaintext_size];
                var bytes_processed = aesgcm_engine.ProcessBytes(encrypted_text, 0, encrypted_text.Length, plaintext, 0);
                aesgcm_engine.DoFinal(plaintext, bytes_processed);
                return plaintext;
            }
        }

        private AsymmetricCipherKeyPair ImportKeys(string pemFileContents, string password)
        {
            using (var string_reader = new StringReader(pemFileContents))
            {
                var pem_reader = new PemReader(string_reader, new ParameterPasswordFinder(password));
                var keys = pem_reader.ReadObject() as AsymmetricCipherKeyPair;
                if (keys == null)
                {
                    throw new ArgumentException("Invalid PEM file contents", nameof(pemFileContents));
                }
                return keys;
            }
        }
        #endregion

        #region Helpers
        private byte[] GenerateRandomBytes(int length)
        {
            var data = new byte[length];
            _random.NextBytes(data);
            return data;
        }

        private byte[] SHA3Hash(byte[] textToHash)
        {
            var sha3_engine = new Sha3Digest();
            var output_size = sha3_engine.GetDigestSize();
            var result = new byte[output_size];
            sha3_engine.BlockUpdate(textToHash, 0, textToHash.Length);
            sha3_engine.DoFinal(result, 0);
            return result;
        }

        private byte[] DeriveBytes(byte[] textToderive, byte[] salt, int iterations = 10000)
        {
            var pdb = new Pkcs5S2ParametersGenerator();
            pdb.Init(textToderive, salt, iterations);
            var result = (KeyParameter)pdb.GenerateDerivedMacParameters(RANDOM_KEY_SIZE * 4);
            return result.GetKey();
        }

        private byte[] SubArray(byte[] source, int index, int length)
        {
            var result = new byte[length];
            Array.Copy(source, index, result, 0, length);
            return result;
        }
        #endregion
    }

}
