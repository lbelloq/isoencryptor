﻿using Org.BouncyCastle.OpenSsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISOEncryptor.Library
{
    public class ParameterPasswordFinder : IPasswordFinder
    {
        private char[] _password;

        public ParameterPasswordFinder(string password)
        {
            _password = password.ToCharArray();
        }

        public char[] GetPassword()
        {
            return _password;
        }
    }
}
